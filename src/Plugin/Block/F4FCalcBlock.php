<?php

/**
 * Defines the Car Rental Form Block.
 */
class F4FCalcBlock extends HfcGlobalBaseBlock {

  /**
   * {@inheritdoc}
   */
  public function info() {
    return [
      'info' => t('F4F calculator form'),
      'cache' => DRUPAL_NO_CACHE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    return t('Futures for Frontliners Calculator');
  }

  /**
   * {@inheritdoc}
   */
  protected function build(&$output) {
    module_load_include('inc', 'f4fcalc', 'forms/f4fcalc_form');
    $output[] = drupal_get_form('f4fcalc_form');
  }

}
