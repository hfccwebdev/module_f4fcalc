<?php

/**
 * Defines the F4F Payment Calculator Service.
 */
class F4FCalcService extends TuitionCalcService {

  /**
   * {@inheritdoc}
   */
  public function calculate(
    string $residency,
    float $credit_hours_assoc,
    float $credit_hours_bach = 0,
    float $course_fees = NULL,
    float $excess_fees = NULL,
    float $bookstore_fees = NULL
  ): array {

    $output = parent::calculate($residency, $credit_hours_assoc, $credit_hours_bach, $course_fees, $excess_fees, $bookstore_fees);

    // The discount is the in-district amount for tuition and base fees.
    $output['f4f_discount'] = parent::calculate('indist', $credit_hours_assoc, $credit_hours_bach)['total_charges'];

    // Per #8177-20, the State of Michigan will cover all in-district
    // excess contact hour fees. Add those to the discount for in-district
    // students, and recalculate the difference for outdistrict.
    $output['f4f_discount'] += ($residency == 'indist')
      ? $excess_fees
      : $excess_fees / self::OUTDISTRICT_ASSOC * self::INDISTRICT_ASSOC;

    $output['f4f_total'] = $output['total_charges'] - $output['f4f_discount'];

    return $output;
  }

}
