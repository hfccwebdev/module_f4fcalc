<?php

/**
 * @file
 * Contains the Futures for Frontliners calculation form.
 *
 * @see f4fcalc.module
 */

/**
 * Defines the Future for Frontliners calculation form.
 */
function f4fcalc_form($form, &$form_state) {

  $form['residency'] = [
    '#type' => 'radios',
    '#title' => t('1. Residency'),
    '#options' => [
      'indist' => t('In-District'),
      'outdist' => t('Out-of-District'),
    ],
    '#required' => TRUE,
  ];

  $form['credit_hours_assoc'] = [
    '#title' => t('2. Level 100 & 200 Credit Hours'),
    '#type' => 'textfield',
    '#description' => t('Only include credits for classes <strong>required</strong> for your program of study (6 credit hours minimum).'),
    '#element_validate' => ['element_validate_number'],
    '#required' => TRUE,
  ];

  $form['credit_hours_bach'] = [
    '#title' => t('3. Level 300 & 400 Credit Hours'),
    '#type' => 'textfield',
    '#element_validate' => ['element_validate_number'],
    '#required' => TRUE,
  ];

  $form['course_fees'] = [
    '#title' => t('4. Course Fees'),
    '#type' => 'textfield',
    '#element_validate' => ['element_validate_number'],
    '#required' => TRUE,
  ];

  $form['excess_fees'] = [
    '#title' => t('5. Excess Contact Hour Fees'),
    '#type' => 'textfield',
    '#element_validate' => ['element_validate_number'],
    '#required' => TRUE,
  ];

  $form['bookstore_fees'] = [
    '#title' => t('6. Inclusive Access Bookstore Fee/Charge'),
    '#type' => 'textfield',
    '#element_validate' => ['element_validate_number'],
    '#required' => TRUE,
  ];

  $form['submit'] = [
    '#type' => 'submit',
    '#value' => 'Calculate Amount',
  ];

  if (!empty($form_state['values']['results'])) {

    $results = $form_state['values']['results'];

    $form['results'] = [
      '#markup' => t('<h2>Estimated Attendance Costs</h2>'),
    ];

    $form['total_charges'] = [
      '#field_name' => 'f4fcalc_total_charges',
      '#type' => 'item',
      '#label' => t('Total Charges (Tuition and Fees)'),
      '#label_display' => 'inline',
      '#markup' => number_format($results['total_charges'], 2),
      '#theme' => 'hfcc_global_pseudo_field',
    ];

    $form['f4f_discount'] = [
      '#field_name' => 'f4f_discount',
      '#type' => 'item',
      '#label' => t('Total Covered (MI-Reconnect or Future for Frontliners Award)'),
      '#label_display' => 'inline',
      '#markup' => number_format($results['f4f_discount'], 2),
      '#theme' => 'hfcc_global_pseudo_field',
    ];

    $form['f4f_total'] = [
      '#field_name' => 'f4f_total',
      '#type' => 'item',
      '#prefix' => '<hr>',
      '#label' => t('Maximum Amount Owed (before additional Financial Aid)'),
      '#label_display' => 'inline',
      '#markup' => number_format($results['f4f_total'], 2),
      '#theme' => 'hfcc_global_pseudo_field',
    ];

  }

  return $form;
}

/**
 * Defines the Future for Frontliners calculation form submit handler.
 */
function f4fcalc_form_submit($form, &$form_state) {

  $values = $form_state['values'];

  $form_state['values']['results'] = F4FCalcService::create()->calculate(
    $values['residency'],
    $values['credit_hours_assoc'],
    $values['credit_hours_bach'],
    $values['course_fees'],
    $values['excess_fees'],
    $values['bookstore_fees']
  );

  $form_state['rebuild'] = TRUE;
}
